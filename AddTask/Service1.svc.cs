﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AddTask
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public void DoWork()
        {
        }

        public string Add_Task(string Nume, string Descriere)
        { 
           string Message;

        SqlConnection con = new SqlConnection("Data Source=(LocalDb)\\MSSQLLocalDB;Database=ProjectContext ");

        con.Open();

            SqlCommand cmd = new SqlCommand("insert into Task(Nume,Descriere,Status,Durata) values(@Nume,@Descriere,'To do',0)", con);

        cmd.Parameters.AddWithValue("@Nume", Nume);

            cmd.Parameters.AddWithValue("@Descriere", Descriere);

            int result = cmd.ExecuteNonQuery();

            if (result == 1)

            {

                Message = Nume + " Details inserted successfully";

            }

            else

            {

                Message = Nume + " Details not inserted successfully";

            }
                con.Close();

            return Message;
        }

    }

}
