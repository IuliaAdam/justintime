﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JustInTime.DAL;
using JustInTime.Models;

namespace JustInTime.Controllers
{
    public class AngajatController : Controller
    {
        private JITContext db = new JITContext();

        // GET: Angajat
        public ActionResult Index()
        {
            return View(db.Angajati.ToList());
        }

        // GET: Angajat/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Angajat angajat = db.Angajati.Find(id);
            if (angajat == null)
            {
                return HttpNotFound();
            }
            return View(angajat);
        }

        // GET: Angajat/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Angajat/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nume,Prenume,User,Parola,Manager")] Angajat angajat)
        {
            if (ModelState.IsValid)
            {
                db.Angajati.Add(angajat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(angajat);
        }

        // GET: Angajat/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Angajat angajat = db.Angajati.Find(id);
            if (angajat == null)
            {
                return HttpNotFound();
            }
            return View(angajat);
        }

        // POST: Angajat/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nume,Prenume,User,Parola,Manager")] Angajat angajat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(angajat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(angajat);
        }

        // GET: Angajat/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Angajat angajat = db.Angajati.Find(id);
            if (angajat == null)
            {
                return HttpNotFound();
            }
            return View(angajat);
        }

        // POST: Angajat/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Angajat angajat = db.Angajati.Find(id);
            db.Angajati.Remove(angajat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
