﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JustInTime.DAL;
using JustInTime.Models;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace JustInTime.Controllers
{
    public class LoginController : Controller
    {
        private JITContext db = new JITContext();
        public ActionResult LoginEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginEmployee(Angajat LoginVar)
        {
            var result1 = db.Angajati.Where(predicate: m => m.User == LoginVar.User && m.Parola == LoginVar.Parola && m.Manager == true).FirstOrDefault();
            var result2 = db.Angajati.Where(predicate: m => m.User == LoginVar.User && m.Parola == LoginVar.Parola && m.Manager == false).FirstOrDefault();

            bool v1 = result1 != null;
            bool v2 = result2 != null;
            if (v1)

            {

                return RedirectToAction("Index", "Home");//I m returning to My Employee Action Method

            }
            if (v2)
            {
                return RedirectToAction("Index", "HomeUser");//I m returning to My Employee Action Method
            }
            else

            {

                ViewBag.Message = string.Format("UserName and Password is incorrect");

                return View();

            }
        }
    }
}