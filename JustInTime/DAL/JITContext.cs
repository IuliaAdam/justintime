﻿using JustInTime.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace JustInTime.DAL
{
    public class JITContext : DbContext
    {
        public JITContext() : base("ProjectContext")
        {
        }

        public DbSet<Angajat> Angajati { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Sprint> Sprints { get; set; }
        public DbSet<Proiect> Proiecte { get; set; }
        public IEnumerable<object> Angajat { get; internal set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}