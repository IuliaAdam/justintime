﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using JustInTime.Models;
namespace JustInTime.DAL
{
    public class JITInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<JITContext>
    {
        protected override void Seed(JITContext context)
        {
            var angajati = new List<Angajat>
{
new Angajat{Nume="Carson",Prenume="Alexander", User="user1@jit.com", Parola="pass", Manager = true},
new Angajat{Nume="Meredith",Prenume="Alonso", User="user2@jit.com", Parola="pass", Manager = false},
new Angajat{Nume="Arturo",Prenume="Anand", User="user3@jit.com", Parola="pass", Manager = false},
new Angajat{Nume="Gytis",Prenume="Barzsdukas", User="user4@jit.com", Parola="pass", Manager = false},
new Angajat{Nume="Yan",Prenume="Li", User="user5@jit.com", Parola="pass", Manager = false},
new Angajat{Nume="Peggy",Prenume="Justice", User="user6@jit.com", Parola="pass", Manager = false},
new Angajat{Nume="Norman",Prenume="Laura", User="user7@jit.com", Parola="pass", Manager = false},
new Angajat{Nume="Nino",Prenume="Olivetto", User="user8@jit.com", Parola="pass", Manager = false},
};
            angajati.ForEach(s => context.Angajati.Add(s));
            context.SaveChanges();
            var tasks = new List<Task>
{
new Task{ID=1050,Nume="Interfata",Status="In lucru",},
new Task{ID=1051,Nume="Baza de date",Status="In lucru",},
new Task{ID=1052,Nume="Creare obiecte",Status="Finalizat",},
};
            tasks.ForEach(s => context.Tasks.Add(s));
            context.SaveChanges();
            var sprints = new List<Sprint>
{
new Sprint{ID=1,Durata=14,},
};
            sprints.ForEach(s => context.Sprints.Add(s));
            context.SaveChanges();
            var proiecte = new List<Proiect>
            {
                new Proiect{ID=1, Nume="MDS", Descriere="Proiect MVC",}
            };

        }
    }
}