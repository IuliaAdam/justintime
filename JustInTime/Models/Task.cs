﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JustInTime.Models
{
    public class Task
    {
        public int ID { get; set; }
        public string Nume { get; set; }
        public string Descriere { get; set; }
        public string Status { get; set; }
        public int Durata { get; set; } //Nr de minute
        public virtual Angajat Angajat { get; set; }//FK catre Angajat
        public virtual Sprint Sprint { get; set; }//FK catre Sprint

    }
}