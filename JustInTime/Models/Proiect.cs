﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JustInTime.Models
{
    public class Proiect
    {
        public int ID { get; set; }
        public string Nume { get; set; }
        public string Descriere { get; set; }
        public virtual ICollection<Sprint> Sprints { get; set; } //Foreign key catre tabela Sprint
    }
}