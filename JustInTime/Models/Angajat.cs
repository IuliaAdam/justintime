﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JustInTime.Models
{
    public class Angajat
    {
        public int ID { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string User { get; set; }
        public string Parola { get; set; }
        public bool Manager { get; set; }

        public virtual ICollection<Task> Tasks { get; set; } // Foreign key catre Task-uri(de tip colectie pt ca un angajat -> mai multe task-uri)

        [NotMapped]
        public SelectList AngajatList { get; set; }
    }
}